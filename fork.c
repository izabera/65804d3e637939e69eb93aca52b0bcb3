#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define elapsed(limit) ({ clock_gettime(CLOCK_MONOTONIC, &current); \
                          current.tv_sec * 1000000000l + current.tv_nsec >= \
                          (start.tv_sec + (limit).tv_sec) * 1000000000l + \
                          start.tv_nsec + (limit).tv_nsec; })
int main(int argc, char *argv[]) {
  struct timespec start, current, limit = { 1, 0 };
  if (argc > 2) {
    limit.tv_sec = atoi(argv[1]);
    limit.tv_nsec = atoi(argv[2]);
  }
  else if (argc > 1) {
    limit.tv_sec = atoi(argv[1]);
    limit.tv_nsec = (atof(argv[1]) - limit.tv_sec) * 1000000000;
  }
  
  clock_gettime(CLOCK_MONOTONIC, &start);
  current = start;
  size_t i = 0;
  while (!(elapsed(limit))) {
    switch (fork()) {
      case  0: _exit(0);
      case -1: if (errno != EAGAIN)
                 err(1, "fork failed after %zu attempts", i);
               wait(NULL);
               continue;
    }
    i++;
  }
  printf("%f fork/s\n", i / (limit.tv_sec + limit.tv_nsec / 1000000000.));
  wait(NULL);
}
